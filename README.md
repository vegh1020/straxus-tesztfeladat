# Tesztfeladat | Végh Bálint

## Az elérhető teszt felhasználók
Ezek a feladatban kért felhasználók, minden be van hozzájuk állítva. A lenti név:jelszó párosokkal lehet használni őket az alkalmazásban.

    Admin:admin
    User 1:123456
    User 2:qwerty
    User 3:ab1cd2

## Adatbázis
Az adatbázis már létre lett hozva egy külső hoszting szerverén, így ezzel sincs mit tenni, de amennyiben másik adatbázist/más kiszolgálót akarunk használni, akkor azt a projekt src/main/resources/application.properties fájljában kell átírni, ám ehhez a módosítás után újra kell buildelni a target mappában levő .jar fájlt mavennel.

> spring.datasource.url=jdbc:mysql://sql7.freemysqlhosting.net:3306/sql7297887	
> spring.datasource.username=sql7297887
> spring.datasource.password=3P75HIytgH

Így néz ki jelenleg az adatbázisr kapcsolatra vonatkozó 3 sor, ha módosítani akarjuk, akkor az alábbi módon kell átírni:

> spring.datasource.url=jdbc:mysql://kiszolgáló/adatbázisnév	
> spring.datasource.username=felhasználónév
> spring.datasource.password=jelszó

Egy esetleges új adatbázisban a táblákat létrehozza, és a fent említett felhasználói, szerepköri adatokat hozzáadja a projekt repository rootjában található straxus.sql fájl.

## Használat

##### Bejelentkezés

Az első fejezetben található felhasználók valamelyikével bejelentkezhetünk. Amíg a bejelentkezés nem történt meg, semmi más nem érhető el, csak a bejelentkezési panel. Amennyiben jól adtuk meg a belépési adatokat a login felületen, átirányít minket az alkalmazás a /home oldalra, ahol az üdvözlőüzenet alatt megtaláljuk az adott felhasználó szerepköreit és az utolsó bejelentkezés dátumát.

#### A felület
Az /admin, /editor és /user oldalakat csak a szükséges szerepkörökkel lehet elérni. A nem elérhető oldalak meg sem jelennek a navigációs sávon, de akkor sem tudjuk elérni őket, ha esetleg mi írjuk be őket manuálisan.

#### Kijelentkezés, session lejárása
A kijelentkezéssel visszakerülünk a bejelentkezés oldalára, ahol újra bejelentkezhetünk ugyanazzal vagy akár másik felhasználóval. 5 perc inaktivitás után automatikusan véget ér a munkamenet, ilyenkor az első adandó requestnél kitessékel minket az alkalmazás a bejelentkezési felületre.

#### Captcha
Háromnál több sikertelen bejelentkezés esetén megjelenik a google recaptchája amit akkor is meg kell oldani, ha a 4. próbálkozásra már helyes adatokat adunk meg. A captcha megoldása nélkül ilyenkor semmilyen módon nem lehet bejelentkezni. A captcha feladat alap esetben csak a doboz bepipálását jelenti, de a google recaptcha apija figyeli a felhasználói aktivitást és ha furcsának érzi a kliens munkásságát, akkor képválasztós feladatot is kiróhat büntetésül.
