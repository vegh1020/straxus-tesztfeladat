package com.testtask.listener;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

/**
 * 
 * @author Végh
 *
 * Azért van rá szükség, hogy az msg attribútumot létrehozza
 * a logout értékkel, így a thymeleaf meg tudja jeleníteni a kijelentkezés
 * üzenetét.
 *
 */
public class CustomLogoutSuccessHandler implements LogoutSuccessHandler
{
	@Override
	public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException
	{
		request.setAttribute("msg", "logout");
		request.getRequestDispatcher("/login").forward(request, response);
	}

}
