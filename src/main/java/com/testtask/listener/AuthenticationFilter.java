package com.testtask.listener;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;

/**
 * 
 * @author Végh
 *
 * Erre az entrypointra azért van szükség, hogy lejáró session esetén
 * megkapja a felhasználó a lejárt munkamenettel kapcsolatos üzenetet.
 * 
 * A commence metódusban továbbítjuk a /login controller felé a requestet,
 * illetve hozzáadjuk a timeout metódust aminek a URL queryje az értéke.
 *
 */

public class AuthenticationFilter extends LoginUrlAuthenticationEntryPoint
{
    public AuthenticationFilter(String url)
    {
        super(url);
    }

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException
    {	
        if (request.getQueryString() != null)
        	request.setAttribute("timeout", "true");
        
        request.getRequestDispatcher("/login").forward(request, response);
    }
}
