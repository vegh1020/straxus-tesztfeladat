package com.testtask.listener;

import java.util.Collection;
import java.util.HashSet;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.testtask.model.Recaptcha;
import com.testtask.model.Role;
import com.testtask.model.User;
import com.testtask.service.UserService;

/**
 * 
 * @author Végh Bálint
 * 
 * - Elvégzi a login formból megkapott captcha paraméter és az 
 * Authentication objektum mezői alapján a hitelesítést.
 * 
 * - Ha a felhasználó nem található, UsernamenotFoundExceptiont dob
 * és leáll az authenticate metódus végrehajtása.
 * 
 * - Ha a felhasználónév és jelszó egyezik, akkor:
 *    > 3 sikertelen bejelentkezési kísérletig hitelesíti.
 *    > Ha 3 sikertelen bejelentkezési kísérleten túl van
 *    a kliens, akkor csak a captcha helyes megoldása függvényében
 *    hitelesíti.
 *  A sikeres hitelesítés után létrehozza a UsernamePasswordAuthentication
 *  tokent ami tartalmazza a felhasználó szerpeköreit is, így be tud lépni
 *  az oldalra és elérheti a szerepköréhez tartozó aloldalakat.  
 *  
 * - Ha a felhasználónév és jelszó nem egyeznek akkor egy
 * BadCredentialsExceptiont dob ami leállítja az authenticate 
 * metódus végrehajtását.
 *
 */
@Component
public class CaptchaAuthenticationProvider implements AuthenticationProvider
{
	@Autowired
	private UserService userService;

	@Autowired
	private HttpServletRequest request;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException
	{
		User user;
		String name = authentication.getName();
		String password = authentication.getCredentials().toString();
		boolean captcha = isCaptchaOkay(request.getParameter("recaptcha"));

		try
		{
			user = userService.getUserByName(name);
		} catch (NullPointerException e)
		{
			throw new UsernameNotFoundException("Nincs '" + name + "' nevű felhasznló az adatbázisban.");
		}

		if (name.equals(user.getName()) && passwordEncoder().matches(password, user.getPassword()) && (user.getLoginAttempts() < 3 || captcha))
		{
			Collection<GrantedAuthority> authorities = new HashSet<>();

			for (Role role : user.getRoles())
				authorities.add(new SimpleGrantedAuthority(role.getName()));

			return new UsernamePasswordAuthenticationToken(name, password, authorities);
		}
		else
		{
			throw new BadCredentialsException("Sikertelen bejelentkezés a(z) '" + name + "' felhasználóval.");
		}
	}

	@Override
	public boolean supports(Class<?> authentication)
	{
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

	@Bean
	public BCryptPasswordEncoder passwordEncoder()
	{
		return new BCryptPasswordEncoder(8);
	}

	public RestTemplate restTemplate()
	{
		return new RestTemplate();
	}
	
	/**
	 * @author Végh Bálint
	 * 
	 * A paraméterként megadjuk a kliens oldalon elhelyezett captcha értékét,
	 * ezt elküldi a google hitelesítő apijának, és az attól JSON-ban kapott 
	 * választ a Recaptcha POJO modelljében meghatározott mezőkbe tölti a RestTemplate segítségével.
	 * 
	 * A visszatérési érték a Recaptcha POJO isSuccess értéke, ez tárolja, hogy a captcha hiteles-e
	 * vagy sem.
	 *
	 */
	private boolean isCaptchaOkay(String response)
	{
		RestTemplate restTpl = restTemplate();
		String baseurl = "https://www.google.com/recaptcha/api/siteverify";
		String secret = "6LfSa6wUAAAAACEO9N1VgFoo2Oa_VuGhX4dC0Dq2\r\n";
		String captchaValidation = baseurl + "?secret= " + secret + "&response=" + response;

		Recaptcha captchaResult = restTpl.exchange(captchaValidation, HttpMethod.POST, null, Recaptcha.class).getBody();

		return captchaResult.isSuccess();
	}
}