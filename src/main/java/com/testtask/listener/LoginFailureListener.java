package com.testtask.listener;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.stereotype.Component;

import com.testtask.model.User;
import com.testtask.service.UserService;

/**
 * 
 * @author Végh Bálint
 * 
 * - A sikertelen hitelesítési kísérletek után fut le.
 * Növeli az attempts mező számát 1-gyel az adatbázisban, annak
 * a felhasználónak a sorában, amivel sikertelen volt a bejelentkezés.
 * Az attemptek számát hozzáfűzi a requesthez.
 * 
 * - A requesthez hozzáfűzi az msg attribútumot, így a login form tetején betöltésre
 * kerül a hibaüzenet.
 *
 */
@Component
public class LoginFailureListener implements ApplicationListener<AuthenticationFailureBadCredentialsEvent>
{

	@Autowired
	private UserService userService;
	
	@Autowired
	private HttpServletRequest request;
	
	@Override
	public void onApplicationEvent(AuthenticationFailureBadCredentialsEvent e)
	{
		User user = userService.getUserByName(e.getAuthentication().getName());
		
		user.setLoginAttempts(user.getLoginAttempts() + 1);
				
		userService.saveUser(user);
		request.setAttribute("attempts", user.getLoginAttempts());
		request.setAttribute("msg", "failed");
	}
}
