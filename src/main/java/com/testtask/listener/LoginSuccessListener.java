package com.testtask.listener;

import java.sql.Timestamp;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

import com.testtask.model.User;
import com.testtask.service.UserService;

/**
 * 
 * @author Végh Bálint
 * 
 * Sikeres bejelentkezés esetén fut le.
 * 
 * - A sessionbe elment a felhasználó nevét, a szerepköreit
 * az utolsó bejelentkezés dátumát és létrehozza az isLoggedIn
 * attribútumot, amiből tudja a LoginController, hogy a felhasználó
 * bejelentkezett már. (username, roles, last_login_date, isLoggedIn)
 * 
 * - A sikertelen kísérletek számát (attempts) lenullázza és elmenti 
 * a jelenlegi bejelentkezés idejét (last_login_date) az adatbázisba. 
 *
 */
@Component
public class LoginSuccessListener implements ApplicationListener<AuthenticationSuccessEvent>
{
	
	@Autowired
	private UserService userService;

	@Autowired
	private HttpSession session;
	
	@Override
	public void onApplicationEvent(AuthenticationSuccessEvent e)
	{
		User user = userService.getUserByName(e.getAuthentication().getName());
	    
		session.setAttribute("username", user.getName());
		session.setAttribute("roles", user.getRoles());
		session.setAttribute("last_login_date", user.getLastLoginDate());
		session.setAttribute("isLoggedIn", true);
		
		user.setLastLoginDate(new Timestamp(System.currentTimeMillis()));		
		user.setLoginAttempts(0);
		userService.saveUser(user);
	}
}
