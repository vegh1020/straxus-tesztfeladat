package com.testtask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppStarter
{	
	/**
	 * 
	 * @author Végh Bálint
	 * 
	 * Elindítja az alkalmazást a jarba épített Tomcat szerveren.
	 * 
	 */
	public static void main(String[] args)
	{
		SpringApplication.run(AppStarter.class, args);

	}

}
