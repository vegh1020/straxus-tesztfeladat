package com.testtask.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.testtask.model.User;

/**
 * 
 * @author Végh Bálint
 * 
 * A User táblával kapcsolatos MySQL műveteleket valósítja meg.
 * A JpaRepository interfész metódusait használja.
 * 
 * - A findByName a felhasználónév alapján kikeresi az adott
 * felhasználót a táblából.
 * 
 * - A save új sort hoz létre, ha pedig létezik a felhasználó már
 * az adatbázisban, akkor frissíti a mezőit.
 *
 */
@Repository
public interface UserRepository<T> extends JpaRepository<User, Integer>
{
	User findByName(String name);

	<S extends User> S save(S user);
}
