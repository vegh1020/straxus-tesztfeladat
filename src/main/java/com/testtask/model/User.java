package com.testtask.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * 
 * @author Végh
 *
 * Az adatbázis users táblájának JPA Entityje.
 * 
 */

@Entity
@Table(name = "users")
public class User implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private int loginAttempts;
	private String name;
	private String password;
	private Timestamp lastLoginDate;

	@ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "user_roles", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
	private Collection<Role> roles;

	public Collection<Role> getRoles()
	{
		return roles;
	}

	public void setRoles(Collection<Role> roles)
	{
		this.roles = roles;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public Timestamp getLastLoginDate()
	{
		return lastLoginDate;
	}

	public void setLastLoginDate(Timestamp lastLoginDate)
	{
		this.lastLoginDate = lastLoginDate;
	}

	public int getLoginAttempts()
	{
		return loginAttempts;
	}

	public void setLoginAttempts(int loginAttempts)
	{
		this.loginAttempts = loginAttempts;
	}

	@Override
	public String toString()
	{
		return "User [id=" + id + ", name=" + name + ", password=" + password + ", lastLoginDate=" + lastLoginDate
				+ ", roles=" + roles.toString() + "]";
	}

}
