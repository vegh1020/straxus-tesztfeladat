package com.testtask.model;

/**
 * 
 * @author Végh Bálint
 *  
 * A google recaptcha API válaszának a mezőit magába
 * foglaló POJO. Ennek a példányát tölti fel a RestTemplate
 *
 */
public class Recaptcha
{
	private boolean success;
	private String challenge_ts;
	private String hostname;
	
	public boolean isSuccess()
	{
		return success;
	}
	public void setSuccess(boolean success)
	{
		this.success = success;
	}
	public String getChallenge_ts()
	{
		return challenge_ts;
	}
	public void setChallenge_ts(String challenge_ts)
	{
		this.challenge_ts = challenge_ts;
	}
	public String getHostname()
	{
		return hostname;
	}
	public void setHostname(String hostname)
	{
		this.hostname = hostname;
	}
}
