package com.testtask.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.testtask.dao.UserRepository;
import com.testtask.model.User;

/**
 * 
 * @author Végh
 *
 * A User entity Service osztálya, meghívja a UserRepository DAO függvényeit.
 * 
 */
@Service
public class UserService
{
	@Autowired
	private UserRepository<User> userRepo;
	
	public User getUserByName (String name)
	{
		User user = userRepo.findByName(name);
		
		return user;
	}
	
	public void saveUser (User user)
	{
		userRepo.save(user);
	}
}
