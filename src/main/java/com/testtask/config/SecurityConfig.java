package com.testtask.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

import com.testtask.listener.AuthenticationFilter;
import com.testtask.listener.CaptchaAuthenticationProvider;
import com.testtask.listener.CustomLogoutSuccessHandler;

@Configuration
@ComponentScan(basePackages = "com.testtask.handler")
public class SecurityConfig extends WebSecurityConfigurerAdapter
{
	
	@Autowired
	private CaptchaAuthenticationProvider authProvider;
		
	/**
	 * 
	 * @author Végh Bálint
	 * 
	 * A Spring Security beállításai:
	 * - Kikapcsolja a csrf védelmet, mert az alkalmazásban ebben a formájában
	 * nem lehet exploitolni semmit csrf támadással.
	 * 
	 * - A roleokat hozzárendeli a saját aloldalaikhoz.
	 * 
	 * - Kizárólag a /login oldal legyen elérhető amíg a felhasználó
	 * be nem jelentkezik.
	 * 
	 * - Hova irányítsa a sikeres(/home) és sikertelen(/login) bejelentkezések után a klienst.
	 * 
	 * - Hogy mikor hozzon létre sessiont. (mindig, tehát például a sikertelen
	 * bejelentkezés után is létrehoz egyet az adott kliensnek, de Principal-t csak akkor fog
	 * tartalmazni, ha sikeres volt a hitelesítés.)
	 * 
	 * - Az egyedi AuthenticationProvider implementációt amire a captcha miatt volt szükség.
	 * 
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception
	{
		http
			.csrf()
				.disable()
			.authorizeRequests()
				.antMatchers("/admin")
					.hasRole("ADMIN")
				.antMatchers("/editor")
					.hasAnyRole("ADMIN", "EDITOR")
				.antMatchers("/user")
					.hasAnyRole("ADMIN", "USER")
				.antMatchers("/**")
					.authenticated()
					.and()
			.httpBasic()
				.and()
			.sessionManagement()
				.invalidSessionUrl("/login?timeout")
				.sessionCreationPolicy(SessionCreationPolicy.ALWAYS)
				.and()
			.formLogin()
				.loginPage("/login")
				.failureForwardUrl("/login")
				.defaultSuccessUrl("/home", true)
				.permitAll()
				.and()
			.logout()
				.logoutUrl("/logout")
				.logoutSuccessHandler(logoutSuccessHandler())
				.invalidateHttpSession(true)
				.and()
			.exceptionHandling()
				.authenticationEntryPoint(new AuthenticationFilter("/login"));
		}


	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception
	{
		auth.authenticationProvider(authProvider);
	}
	
	@Bean
	public CustomLogoutSuccessHandler logoutSuccessHandler()
	{
	    return new CustomLogoutSuccessHandler();
	}


}
