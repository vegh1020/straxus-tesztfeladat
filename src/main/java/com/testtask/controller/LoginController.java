package com.testtask.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 
 * @author Végh Bálint
 * 
 *         A login oldal Controller osztálya. - Amennyiben a felhasználó, már be
 *         van jelentkezve átirányítja a /home oldalra.
 * 
 *         - Az msg attribútumot a failureListenertől és a logoutHandlertől
 *         kapja meg.
 * 
 *         - Ha a failureListenertől megkapta a "failed" értéket tartalmazó msg
 *         attribútumot, akkor átadja a thymeleafnek az attempt értékét. Ez
 *         ahhoz kell, hogy 3 sikertelen próbálkozás felett megjelenhessen a
 *         captcha.
 *
 */
@Controller
public class LoginController
{
	@RequestMapping(value = "/login")
	public String loginPage(Model model, HttpSession session, HttpServletRequest request)
	{
		if (session.getAttribute("isLoggedIn") != null)
			return "redirect:/home";

		String msg = "";

		if (request.getAttribute("msg") != null)
			msg = (String) request.getAttribute("msg");

		if (msg.equals("failed"))
			model.addAttribute("attempts", request.getAttribute("attempts"));

		model.addAttribute("msg", msg);

		return "login";
	}
}
