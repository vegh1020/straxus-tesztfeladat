package com.testtask.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 
 * @author Végh Bálint
 * 
 * - A getPages metódus az URL-ben levő érték alapján betölti
 * vagy az adott oldal thymeleaf fragmentjét vagy amennyiben
 * nem létezik az oldal a 404 hiba felhasználóbarátabb kiírását.
 * 
 * - A nonPages metódus átirányítja a 404 oldalra az összes olyan
 * requestet amit a getPages nem 'kap el'.
 *
 */
@Controller
public class PageController
{	
	@RequestMapping(value = "/*")
	public String nonPages(HttpServletRequest request)
	{		
		return "redirect:/404";
	}
	
	@RequestMapping(value = "/{page}")
	public String getPages(Model model, HttpServletRequest request, @PathVariable(name = "page") String page)
	{		
		switch (page)
		{
		case "home":
			model.addAttribute("page", "home");
			break;
		case "admin":
			model.addAttribute("page", "admin");
			break;
		case "editor":
			model.addAttribute("page", "editor");
			break;
		case "user":
			model.addAttribute("page", "user");
			break;
		default:
			model.addAttribute("page", "404");
			break;
		}
				
		return "index";
	}
}
